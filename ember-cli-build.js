'use strict';

const EmberAddon = require('ember-cli/lib/broccoli/ember-addon');
const buildJsonModules = require('./lib/build-json-modules');

module.exports = function(defaults) {
  let app = new EmberAddon(defaults, {
    // Add options here
  });

  /*
    This build file specifies the options for the dummy test app of this
    addon, located in `/tests/dummy`
    This build file does *not* influence how the addon or the app using it
    behave. You most likely want to be modifying `./index.js` or app's build file
  */

  let trees = [];
  if (EmberAddon.env() === 'test') {
    let fixtures = buildJsonModules('tests/dummy/fixtures', 'assets');
    trees.push(fixtures);
  }
  return app.toTree(trees);
};
