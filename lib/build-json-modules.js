let Funnel = require('broccoli-funnel');
let jsonToModule = require('broccoli-json-module');
let esTranspiler = require('broccoli-babel-transpiler');
let concat = require('broccoli-concat');

/**
 * @method buildJsonModules
 * @param {String} sourceFolder name of folder in root of repo, no nesting, e.g. 'fixtures'
 * @param {String} destinationFolder name of folder within final dist, e.g. 'assets'
 * @return {Object} node concatination of .json modules as a single output file destination/source.js
 */
module.exports = function buildJsonModules(sourceFolder, destinationFolder) {
  let jsonNode = new Funnel(sourceFolder, {
    include: ['**/*.json'],
    destDir: `/${destinationFolder}`
  });
  let jsonModules = jsonToModule(jsonNode);
  let transpiled = esTranspiler(jsonModules, {
    modules: 'amd',
    moduleIds: true,
    getModuleId: function (name) {
      // name the module begining with the source folder instead of destination
      return name.replace(new RegExp(destinationFolder), sourceFolder);
    }
  });
  return concat(transpiled, {
    outputFile: `/${destinationFolder}/${sourceFolder}.js`,
    sourceMapConfig: { enabled: false }
  });
}
