export default function stringify(moduleDefaultObject) {
  return JSON.stringify(moduleDefaultObject, circularReplacer, 1);
}

function circularReplacer(key, val) {
  // AMD default import as object has circular reference to `default`
  return (val === this) ? undefined : val;
}
