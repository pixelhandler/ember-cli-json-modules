ember-cli-json-modules
==============================================================================

See this gist, https://gist.github.com/pixelhandler/51cfcb39a78d35f14bb84debd235ce46

Place your JSON files in a `fixtures` as sibling to `test`, then import like so:

Add to `tests/index.html`:
```html
<script src="{{rootURL}}assets/fixtures.js"></script>
```
And in a test file, with example XHR mock via Sinon.JS:
```js
import fixture from 'fixtures/sessions/current-user';
import stringify from 'app-name/tests/helpers/stringify-amd';

this.sandbox.server.respondWith(
  'GET',
  `${rootUrl}/sessions/current-user`,
  [200, { "Content-Type": "application/json; charset=utf-8" }, stringify(fixture)]
);
```

You'll need some dependencies in your package.json:
```json
    "broccoli-babel-transpiler": "^5.7.1",
    "broccoli-concat": "^3.2.2",
    "broccoli-es6-concatenator": "^0.1.11",
    "broccoli-funnel": "^1.2.0",
    "broccoli-json-module": "^1.0.0",
```

Installation
------------------------------------------------------------------------------

```
ember install ember-cli-json-modules
```


Usage
------------------------------------------------------------------------------

[Longer description of how to use the addon in apps.]


Contributing
------------------------------------------------------------------------------

### Installation

* `git clone <repository-url>`
* `cd ember-cli-json-modules`
* `npm install`

### Linting

* `npm run lint:js`
* `npm run lint:js -- --fix`

### Running tests

* `ember test` – Runs the test suite on the current Ember version
* `ember test --server` – Runs the test suite in "watch mode"
* `ember try:each` – Runs the test suite against multiple Ember versions

### Running the dummy application

* `ember serve`
* Visit the dummy application at [http://localhost:4200](http://localhost:4200).

For more information on using ember-cli, visit [https://ember-cli.com/](https://ember-cli.com/).

License
------------------------------------------------------------------------------

This project is licensed under the [MIT License](LICENSE.md).
